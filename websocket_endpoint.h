#ifndef _WEBSOCKET_ENDPOINT_H_
#define _WEBSOCKET_ENDPOINT_H_
#include <string>

class websocket_endpoint;

class CWsClient
{
public:
    CWsClient();
public:
    bool Connect(const std::string &uri);
    void Close();
    void Send(const std::string &message);
private:
    static websocket_endpoint m_wsEndpoint;
    int m_wsId;
};
#endif

